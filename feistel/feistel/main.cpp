#include <iostream>
#include <vector>
typedef unsigned long long uint64_t;
typedef unsigned int uint32_t;
#define N 8
#define F32 0xFFFFFFFF
#define size64 sizeof(uint64_t)
#define ROR(x,n,xsize)((x>>n)|(x<<(xsize-n)))
#define ROL(x,n,xsize)((x<<n)|(x>>(xsize-n)))
//#define RKEY(r)((ROR(K,r*3,size64*8))&F32)
const uint64_t K = 0x96EA704CFB1CF672;//base key to forming of round keys
uint32_t RK[N];//massive round keys
#define CBC 0
#define CFB 0
#if CBC == 1 || CFB ==1
	const uint64_t IV = 0x96EA704CFB1CF672;
#endif
void createRoundKeys()
{
	for (int i = 0; i < N; i++)
		RK[i]=(ROR(K, i * 8, size64 * 8))&F32;
}
uint32_t F(uint32_t subblk, uint32_t key)
{
	/*
	uint32_t f1 = ROL(subblk, 9, sizeof(subblk) * 8);
	uint32_t f2 = ROR(key, 11, sizeof(key) * 8) | subblk;
	return f1 ^ !f2;
	*/
	return subblk+key;
}
uint64_t encrypt(uint64_t block)
{
	//select subblocks
	uint32_t left = (block >> 32)&F32;
	uint32_t right = block&F32;
	uint32_t left_, right_;//subblock in the end of round
	for (int r = 0; r < N; r++)
	{
		uint32_t fk = F(left, RK[r]);
		left_ = left;
		right_ = right^fk;
		if (r < N - 1)//swap places to next round
		{
			left = right_;
			right = left_;
		}
		else//last round not swap
		{
			left = left_;
			right = right_;
		}
	}
	//collect subblock in block
	uint64_t c_block = left;
	c_block = (c_block << 32) | (right&F32);
	return c_block;
}
uint64_t decrypt(uint64_t c_block)
{
	//select subblocks
	uint32_t left = (c_block >> 32)&F32;
	uint32_t right = c_block&F32;
	uint32_t left_, right_;//subblock in the end of round
	for (int r = N-1; r >=0; r--)
	{
		uint32_t fk = F(left, RK[r]);
		left_ = left;
		right_ = right^fk;
		if (r > 0)//swap places to next round
		{
			left = right_;
			right = left_;
		}
		else //last round not swap
		{
			left = left_;
			right = right_;
		}
	}
	//collect subblock in block
	uint64_t block = left;
	block = (block << 32) | (right&F32);
	return block;
}
#pragma warning(disable:4996)
void main()
{
	createRoundKeys();
	FILE *fp,*fencrypted,*fdecrypted;
	char str[N+1];
	str[N] = '\0';
	if ((fp=fopen("message.txt", "rb" ))==NULL)
		exit (1);
	std::vector<uint64_t>* msg = new std::vector<uint64_t>(),
		* plaintext = new std::vector<uint64_t>()
#if CBC == 1 && CFB ==0
		, * cipherback = new std::vector<uint64_t>()
#endif
		;
	unsigned long long id;
	while (fread(str, sizeof(char), N, fp) > 0)
	{
		memcpy(&id, str, N);
		msg->push_back(id);
		memset(str, 0, N);
	}
	fclose(fp);
	if ((fencrypted = fopen("crypt_message.txt", "wb")) == NULL)
		exit(1);
#if CBC == 1 || CFB == 1
	uint64_t iv = IV;
#endif
	uint64_t cipher;
	for (std::vector<uint64_t>::iterator it = msg->begin(); it != msg->end(); ++it)
	{
#if CBC == 0 && CFB == 0
		cipher = encrypt(*it);
#endif
#if CBC == 1 && CFB ==0
		cipher = encrypt(*it ^ iv);//change on true second parameter when debug, ciphertext
		iv = cipher;
#endif
#if CBC == 0 && CFB == 1
		cipher = encrypt(iv);
		cipher ^= *it;
		iv = cipher;
#endif
		memcpy(str, &cipher, N);
		fwrite(str, sizeof(char), N, fencrypted);
	}
	fclose(fencrypted);
#if CBC == 1 || CFB ==1
	iv = IV;
	int i = 0;
#endif
	if ((fencrypted = fopen("crypt_message.txt", "rb")) == NULL)
		exit(1);
	
	while (fread(str, sizeof(char), N, fencrypted)>0)
	{
		memcpy(&id, str, N);
#if CBC == 0 && CFB == 0
		plaintext->push_back(decrypt(id));
#endif
#if CBC == 1 && CFB ==0
		cipherback->push_back(id);
		plaintext->push_back(decrypt(id));
		plaintext->at(i) ^= iv;
		iv = cipherback->at(i);
		i++;
#endif
#if CBC == 0 && CFB == 1
		plaintext->push_back(encrypt(iv) ^ id);
		iv = id;
#endif
	}
	fclose(fencrypted);
	if ((fdecrypted = fopen("decrypt_message.txt", "wb")) == NULL)
		exit(1);
	for (std::vector<uint64_t>::iterator it = plaintext->begin(); it != plaintext->end(); ++it)
	{
		memcpy(str, &*it, N);
		fwrite(str, sizeof(char), strlen(str), fdecrypted);
	}
	fclose(fdecrypted);
}