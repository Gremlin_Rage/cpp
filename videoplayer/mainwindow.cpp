#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVideoWidget>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Player");
    //qp->setParent(this);
    m_videoWidget = new QVideoWidget(this);
    ui->gridLayout->addWidget(m_videoWidget);
    qp = new QMediaPlayer(m_videoWidget);
    qp->setVideoOutput(m_videoWidget);
    m_videoWidget->show();
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(slotTimerAlarm()));
    timer->start(1000);
    connect(qp, &QMediaPlayer::durationChanged, this, [&](qint64 dur) {
        ui->horizontalSlider->setMinimum(0);
        ui->horizontalSlider->setMaximum((int)qp->duration()/1000);
    });
    ui->verticalSlider->setMinimum(0);
    ui->verticalSlider->setMaximum(100);
    ui->verticalSlider->setValue(100);
    model = new QStringListModel(list);
}
MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_pushButton_clicked()
{
    qp->play();
}
void MainWindow::on_pushButton_2_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Choose File");
    list.append(filename);
    model = new QStringListModel(list);
    ui->listView->setModel(model);
}
void MainWindow::on_pushButton_3_clicked()
{
    qp->stop();
}
void MainWindow::on_pushButton_4_clicked()
{
    qp->pause();
}
void MainWindow::slotTimerAlarm()
{
    ui->horizontalSlider->setValue((int)qp->position()/1000);
}

void MainWindow::on_verticalSlider_valueChanged(int value)
{
    qp->setVolume(ui->verticalSlider->value());
}


void MainWindow::on_listView_clicked(const QModelIndex &index)
{
    ui->listView->setCurrentIndex(index);
    qp->setMedia(QUrl::fromLocalFile(ui->listView->currentIndex().data(Qt::DisplayRole).toString()));
    qp->play();
}

