#include "MyWidget.h"
MyWidget::MyWidget()
{
}
void MyWidget::sectionClicked(int row, int column)
{
    //std::cout << "row:" << row << " column:" << column << std::endl;
}
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlerror.h>
#include <QtSql/qsqlquery.h>

#include <iostream>
#pragma comment(lib,"Qt6Sqld.lib")

void MyWidget::InitWidget()
{
    QSqlDatabase dbase = QSqlDatabase::addDatabase("QODBC");
    //dbase.setDatabaseName("Driver={Microsoft dBase Driver (*.dbf)};SourceType=DBF;");
    dbase.setDatabaseName("Driver={Microsoft dBase Driver (*.dbf)};SourceType=DBF;");
    if (!dbase.open()) {
        setlocale(LC_ALL, "ru_RU.UTF-8");
        std::cout<< "������ SQL>" << dbase.lastError().text().toStdString();
        return;
    }
    QSqlQuery query(dbase);
    query.exec("CREATE TABLE D:\\TABLE(field integer, field2 integer)");
    windowsettings.setWindowTitle("Settings");
    windowsettings.setGeometry(0, 0, 300, 500);
    
    QLabel* labelhost = new QLabel("ip address remote host");
    labelhost->setParent(&windowsettings);
    labelhost->setGeometry(10,10,200, 25);
    QTextEdit* hostnameedit = new QTextEdit();
    hostnameedit->setParent(&windowsettings);
    hostnameedit->acceptRichText();
    QString hostname = "localhost";
    hostnameedit->setText(hostname);
    hostnameedit->setGeometry(10, 35, 200, 25);
    hostnameedit->setFixedSize(200, 25);
 
    QLabel* labeluser = new QLabel("username");
    labeluser->setGeometry(10, 60, 200, 25);
    labeluser->setParent(&windowsettings);
    QTextEdit*usernameedit = new QTextEdit();
    usernameedit->acceptRichText();
    QString username = "postgres";
    usernameedit->setText(username);
    usernameedit->setGeometry(10, 85, 200, 25);
    usernameedit->setFixedSize(200, 25);
    usernameedit->setParent(&windowsettings);

    QLabel* labelpass = new QLabel("password");
    labelpass->setGeometry(10, 110, 200, 25);
    labelpass->setParent(&windowsettings);
    QTextEdit*passwordedit = new QTextEdit();
    passwordedit->acceptRichText();
    QString   password = "root";
    passwordedit->setText(password);
    passwordedit->setGeometry(10, 135, 200, 25);
    passwordedit->setFixedSize(200, 25);
    passwordedit->setParent(&windowsettings);

    QLabel*labeldb = new QLabel("database");
    labeldb->setGeometry(10, 160, 200, 25);
    labeldb->setParent(&windowsettings);
    QTextEdit* databaseedit = new QTextEdit();
    databaseedit->acceptRichText();
    QString database = "postgres";
    databaseedit->setText(database);
    databaseedit->setGeometry(10, 185, 200, 25);
    databaseedit->setFixedSize(200, 25);
    databaseedit->setParent(&windowsettings);

    QLabel*labelchar = new QLabel("charset");
    labelchar->setGeometry(10, 210, 200, 25);
    labelchar->setParent(&windowsettings);
    QTextEdit* charsetedit = new QTextEdit();
    charsetedit->acceptRichText();
    QString charset = "utf8mb4";
    charsetedit->setText(charset);
    charsetedit->setGeometry(10, 235, 200, 25);
    charsetedit->setFixedSize(200, 25);
    charsetedit->setParent(&windowsettings);
            
    QPushButton* buttonc =new  QPushButton("Connect to database",&windowsettings);
    buttonc->setToolTip("This is a connecting button");
    buttonc->setGeometry(10, 260, 200, 25);
    QObject::connect(buttonc, &QPushButton::clicked, [=]() {
        db.Connect(usernameedit->toPlainText().toStdString(), passwordedit->toPlainText().toStdString(), hostnameedit->toPlainText().toStdString(), databaseedit->toPlainText().toStdString());
        self.setWindowTitle("Med");
        self.setGeometry(50, 50, 1000, 700);

        db.StandartQuery();
        labelworkers.setText("Persons");
        labelworkers.setGeometry(0, 0, 50, 30);
        labelworkers.setParent(&self);
        tableworkers.setParent(&self);
        tableworkers.setColumnCount(db.numCol);
        tableworkers.setRowCount(db.numRow);
        tableworkers.setGeometry(0, 30, 950, 150);
        Tape(tableworkers, 0);
        //QObject::connect(&tableworkers, &QTableWidget::cellActivated, this, &MyWidget::sectionClicked);
        self.show();
        QObject::connect(&tableworkers, &QTableWidget::cellClicked, [this](int row, int column) {
            sectionClicked(row, column);
            db.Query(row);
            RowPolis = row;
            tableevents.setVisible(false);
            labelpolises.setText("Polises");
            labelpolises.setGeometry(0, 180, 50, 30);
            labelpolises.setParent(&self);
            labelpolises.show();
            tablepolises.setColumnCount(db.numCol);
            tablepolises.setRowCount(db.numRow);
            tablepolises.setGeometry(0, 210, 950, 150);
            tablepolises.setParent(&self);
            tablepolises.show();
            Tape(tablepolises, 1);
            QObject::connect(&tablepolises, &QTableWidget::cellClicked, [this](int row, int column) {
                sectionClicked(row, column);
                db.FinalQuery(RowPolis);
                labelevents.setText("Events");
                labelevents.setGeometry(0, 360, 50, 30);
                labelevents.setParent(&self);
                labelevents.show();
                tableevents.setColumnCount(db.numCol);
                tableevents.setRowCount(db.numRow);
                tableevents.setGeometry(0, 390, 950, 150);
                tableevents.setParent(&self);
                tableevents.show();
                Tape(tableevents, 2);
                });
            });
        //buttonc->setText("Hi");
    });
    
    QPushButton* buttoncc = new QPushButton("Close connect", &windowsettings);
    buttoncc->setToolTip("This is a close connection button");
    buttoncc->setGeometry(10, 285, 200, 25);
    QObject::connect(buttoncc, &QPushButton::clicked, [=]() {
        db.terminate(0);
        //buttoncc->setText("Hi");
        });
    windowsettings.show();
}

void MyWidget::Tape(QTableWidget& table, int t)
{
    QStringList list;
    QString qstr;
    for (int i = 0; i < db.namescol.size(); i++)
    {
        qstr = QString::fromStdString(db.namescol[i]);
        list.push_back(qstr);
    }
    table.setHorizontalHeaderLabels(list);
    vector<vector<string>> v;
    vector<string> tmp;
    const int n = 64;
    char* date_i = new char[n],
        * date_s = new char[n], * strid = new char[n];
    char* strid_p = new char[n];
    memset(date_i, 0, n);
    memset(date_s, 0, n);
    switch (t) {
        case 0: {
            for (int i = 0; i < db.vWorker.size(); i++)
            {
                itoa(db.vWorker[i].id, strid, 10);
                tmp.push_back(strid);
                tmp.push_back(db.vWorker[i].secondname);
                tmp.push_back(db.vWorker[i].firstname);
                tmp.push_back(db.vWorker[i].patronymic);
                std::string tmpstr = "";
                tmpstr += std::to_string(1900 + db.vWorker[i].datebirth->tm_year) + "-";
                tmpstr += std::to_string(1 + db.vWorker[i].datebirth->tm_mon) + "-";
                tmpstr += std::to_string(db.vWorker[i].datebirth->tm_mday);
                //tmpstr += std::to_string(db.vWorker[i].datebirth->tm_hour)+":";
                //tmpstr += std::to_string(db.vWorker[i].datebirth->tm_min)+":";
                //tmpstr += std::to_string(db.vWorker[i].datebirth->tm_sec);
                tmp.push_back(tmpstr);
                v.push_back(tmp);
                tmp.clear();
            }
            break;
        }
        case 1: {
            for (int i = 0; i < db.numRow; i++)
            {
                itoa(db.vPolis[i].id, strid, 10);
                tmp.push_back(strid);
                itoa(db.vPolis[i].id_person, strid_p, 10);
                tmp.push_back(strid_p);
                tmp.push_back(db.vPolis[i].number_polis);
                strftime(date_i, n, " %Y-%m-%d", db.vPolis[i].date_issuance);
                tmp.push_back(date_i);
                strftime(date_s, n, "%Y-%m-%d", db.vPolis[i].date_seizure);
                tmp.push_back(date_s);
                v.push_back(tmp);
                tmp.clear();
            }
            break;
        }
        case 2:{
            for (int i = 0; i < db.numRow; i++)
            {
                itoa(db.vEvents[i].id, strid, 10);
                tmp.push_back(strid);
                itoa(db.vEvents[i].id_polis, strid_p, 10);
                tmp.push_back(strid_p);
                tmp.push_back(db.vEvents[i].event);
                strftime(date_i, n, " %Y-%m-%d", db.vEvents[i].date_event);
                tmp.push_back(date_i);
                v.push_back(tmp);
                tmp.clear();
            }
            break;
        }
    }
    for (int i = 0; i < v.size(); i++)
        for (int j = 0; j < v[i].size(); j++)
        {
            qstr = QString::fromStdString(v[i][j]);
            table.setItem(i, j, new QTableWidgetItem(qstr));
        }
}
MyWidget::~MyWidget()
{
}
