#pragma once
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <libpq-fe.h>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip> 
#include "Polis.h"
#include "Worker.h"
#include "Events.h"
#pragma comment(lib,"libpq.lib")
#define UNUSED(x) (void)(x)
using namespace std;
class DatabaseConnection
{
public:
    PGconn* conn = NULL;
    PGresult* res = NULL;
    int numCol, numRow;
    vector<string> namescol;
    Polis vPolis[1025];
    vector<Worker> vWorker;
    Events vEvents[1025];
    char** ddl, ** ddl2;
    DatabaseConnection();
    void Connect(std::string user_, std::string pass_, std::string host_, std::string dbaname_);
    void terminate(int code);
    void clearRes();
    static void processNotice(void* arg, const char* message);
    void Query(int row);
    void StandartQuery();
    void FinalQuery(int row);
    ~DatabaseConnection();
};


