#pragma once
#include <QtWidgets/qtablewidget.h>
#include <QtWidgets/qlabel.h>
#include <QtWidgets/qtextedit.h>
#include <QtWidgets/qpushbutton.h> 
#include <QtWidgets/qlayout.h>
#include "DatabaseConnection.h"

#pragma comment(lib,"Qt6Cored.lib")
#pragma comment(lib,"Qt6Widgetsd.lib")

class MyWidget
{
public:
	QWidget self;
	QTableWidget tableworkers;
	QTableWidget tablepolises;
	QTableWidget tableevents;
	QLabel labelworkers;
	QLabel labelpolises;
	QLabel labelevents;
	QTextEdit editid, edittask, editparenttask, editexecutor, editdeath;
	QPushButton buttondelete, buttonrefresh, addbutton;
	MyWidget();
	DatabaseConnection db;

	void InitWidget();
	void Tape(QTableWidget& table,int t);
	QWidget windowsettings;
	//QLabel *labelhost;
	void sectionClicked(int row,int column);
	int RowPolis;
	~MyWidget();

};

