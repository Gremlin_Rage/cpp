#include "DatabaseConnection.h"
DatabaseConnection::DatabaseConnection()
{
}
void DatabaseConnection::Connect(std::string user_, std::string pass_, std::string host_, std::string dbname_)
{
    int libpq_ver = PQlibVersion();
    printf("Version of libpq: %d\n", libpq_ver);
    std::string args = "user=" + user_ + " password=" + pass_ + " host=" + host_ + " dbname=" + dbname_;
    std::cout << args<<std::endl;
    conn = PQconnectdb(args.c_str());
    //conn = PQconnectdb("user=postgres password=root host=127.0.0.1 dbname=postgres");
    if (PQstatus(conn) != CONNECTION_OK)
        terminate(1);
    PQsetNoticeProcessor(conn, processNotice, NULL);

    int server_ver = PQserverVersion(conn);
    char* user = PQuser(conn);
    char* db_name = PQdb(conn);

    printf("Server version: %d\n", server_ver);
    printf("User: %s\n", user);
    printf("Database name: %s\n", db_name);
}
void DatabaseConnection::StandartQuery()
{
    res = PQexec(conn, "SELECT * FROM persons;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        terminate(1);

    int ncols = PQnfields(res);
    numCol = ncols;
    printf("There are %d columns:", ncols);
    namescol.clear();
    for (int i = 0; i < ncols; i++)
    {
        char* name = PQfname(res, i);
        namescol.push_back(name);
        printf(" %s", name);
    }
    printf("\n");

    int nrows = PQntuples(res);
    numRow = nrows;
    Worker* worker_ = new Worker[nrows];
    ddl = new char* [nrows], ddl2 = new char* [nrows];
    for (int i = 0; i < nrows; i++)
    {
        worker_[i].id = atoi(PQgetvalue(res, i, 0));
        strcpy(worker_[i].secondname, PQgetvalue(res, i, 1));
        strcpy(worker_[i].firstname, PQgetvalue(res, i, 2));
        strcpy(worker_[i].patronymic, PQgetvalue(res, i, 3));
        ddl[i] = new char[100];
        strcpy(ddl[i], PQgetvalue(res, i, 4));
        ddl[i][4] = ddl[i][7] = '\0';
        worker_[i].datebirth->tm_year = atoi(&ddl[i][0]) - 1900;
        worker_[i].datebirth->tm_mon = atoi(&ddl[i][5]) - 1;
        worker_[i].datebirth->tm_mday = atoi(&ddl[i][8]);
        //cout << " Id: " << worker_[i].id;
        //cout << " Name: " << worker_[i].secondname<< worker_[i].firstname<< worker_[i].patronymic;
        //cout << " DateBirth: ";
        //cout << "Year:" << 1900 + worker_[i].datebirth->tm_year;
        //cout << "Month: " << 1 + worker_[i].datebirth->tm_mon;
        //cout << "Day: " << worker_[i].datebirth->tm_mday;
        //cout << "Time: " << worker_[i].datebirth->tm_hour << ":";
        //cout << worker_[i].datebirth->tm_min << ":";
        //cout << worker_[i].datebirth->tm_sec << endl;

        vWorker.push_back(worker_[i]);
    }
    printf("Total: %d rows\n", nrows);
    clearRes();
}
void DatabaseConnection::Query(int row)
{
    std::string query = "SELECT * from polises where id_person=";
    query += std::to_string(row+1);
    query+=";";
    res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        terminate(1);

    int ncols = PQnfields(res);
    numCol = ncols;
    printf("There are %d columns:", ncols);
    namescol.clear();
    for (int i = 0; i < ncols; i++)
    {
        char* name = PQfname(res, i);
        namescol.push_back(name);
        //  printf(" %s", name);
    }
    //printf("\n");
    int nrows = PQntuples(res);
    numRow = nrows;
    Polis*polis_ = new Polis[nrows];
    ddl = new char* [nrows], ddl2 = new char* [nrows];
    for (int i = 0; i < nrows; i++)
    {
        polis_[i].id = atoi(PQgetvalue(res, i, 0));
        polis_[i].id_person = atoi(PQgetvalue(res, i, 1));
        strcpy(polis_[i].number_polis, PQgetvalue(res, i, 2));
        ddl[i] = new char[100];
        strcpy(ddl[i], PQgetvalue(res, i, 3));
        ddl[i][4] = ddl[i][7] = '\0';
        polis_[i].date_issuance->tm_year = atoi(&ddl[i][0]) - 1900;
        polis_[i].date_issuance->tm_mon = atoi(&ddl[i][5]) - 1;
        polis_[i].date_issuance->tm_mday = atoi(&ddl[i][8]);

        ddl2[i] = new char[100];
        strcpy(ddl2[i], PQgetvalue(res, i, 4));
        ddl2[i][4] = ddl2[i][7] = '\0';
        polis_[i].date_seizure->tm_year = atoi(&ddl2[i][0]) - 1900;
        polis_[i].date_seizure->tm_mon = atoi(&ddl2[i][5]) - 1;
        polis_[i].date_seizure->tm_mday = atoi(&ddl2[i][8]);
        vPolis[i] = polis_[i];
    }
    printf("Total: %d rows\n", nrows);
    clearRes();
}
void DatabaseConnection::FinalQuery(int row)
{
    std::string query = "SELECT * from events where id_polis=";
    query += std::to_string(row + 1);
    query += ";";
    res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        terminate(1);

    int ncols = PQnfields(res);
    numCol = ncols;
    printf("There are %d columns:", ncols);
    namescol.clear();
    for (int i = 0; i < ncols; i++)
    {
        char* name = PQfname(res, i);
        namescol.push_back(name);
        //  printf(" %s", name);
    }
    //printf("\n");
    int nrows = PQntuples(res);
    numRow = nrows;
    Events* events_ = new Events[nrows];
    ddl = new char* [nrows], ddl2 = new char* [nrows];
    for (int i = 0; i < nrows; i++)
    {
        events_[i].id = atoi(PQgetvalue(res, i, 0));
        events_[i].id_polis = atoi(PQgetvalue(res, i, 1));
        strcpy(events_[i].event, PQgetvalue(res, i, 2));
        ddl[i] = new char[100];
        strcpy(ddl[i], PQgetvalue(res, i, 3));
        ddl[i][4] = ddl[i][7] = '\0';
        events_[i].date_event->tm_year = atoi(&ddl[i][0]) - 1900;
        events_[i].date_event->tm_mon = atoi(&ddl[i][5]) - 1;
        events_[i].date_event->tm_mday = atoi(&ddl[i][8]);

        vEvents[i] = events_[i];
    }
    printf("Total: %d rows\n", nrows);
    clearRes();
}
void DatabaseConnection::processNotice(void* arg, const char* message)
{
    UNUSED(arg);
    UNUSED(message);
}
void DatabaseConnection::terminate(int code)
{
    setlocale(LC_ALL, "rus");
    ofstream fout("errors.txt", ios_base::out | ios_base::trunc);
    if (!fout.is_open()) //    
    {
        cout << "Can't open errors.txt\n";  
    }
    fout << PQerrorMessage(conn);
    fout.close();
    if (code != 0)
        fprintf(stderr, "%s\n", PQerrorMessage(conn));
    if (res != NULL)
        PQclear(res);
    if (conn != NULL)
        PQfinish(conn);
    exit(code);
}
void DatabaseConnection::clearRes()
{
    PQclear(res);
    res = NULL;
}
DatabaseConnection::~DatabaseConnection()
{
    terminate(0);
}
