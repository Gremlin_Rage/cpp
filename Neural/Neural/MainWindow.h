#pragma once
#include <QtWidgets/qmainwindow.h>
#include <QtGui/qpicture.h>
#include <QtGui/qimage.h>
#include <QtGui/qpainter.h>
#include <QtCore/qdebug.h> 
#include <vector>
#include <iostream>
#include <fstream> // ������ � �������
#include <iomanip> 
#include "NeuralNet.h"
#pragma comment(lib,"Qt5Core.lib")
#pragma comment(lib,"Qt5Widgets.lib")
#pragma comment(lib,"Qt5Gui.lib")

namespace Ui {
    class MainWindow;
}
//Q_OBJECT
class MainWindow : public QMainWindow
{
    //Q_OBJECT
public:
    explicit MainWindow(QWidget* parent = 0);
    void CreateImage(QString path);
    void CreateImage2(QString path);
    void OpenImage(QString path);
    void OpenImage2(QString path);
    std::vector<double> x1, x2, zeros1, zeros2;
    QImage* image_t1, * image_t2 ;
    NeuralNet net;
    // ~MainWindow();
protected:
    void paintEvent(QPaintEvent*); // �������������� ����������� �������
    
private:
    Ui::MainWindow* ui;
    
};