#pragma once
#include "Neuron.h"
//#include <vector>
class NeuralNet
{
public:
	std::vector<Neuron*> l1,l2;
	void InitNeurons(std::vector<double> x, int length);
	void LearnNeurons(int steps, int i);
	double TestNeurons(int i, std::vector<double> xx);
};

