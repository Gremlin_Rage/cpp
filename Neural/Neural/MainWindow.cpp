#include "MainWindow.h"
MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
{
    setWindowTitle(tr("Neural"));
    setGeometry(0, 0, 1000, 700);
}
void MainWindow::paintEvent(QPaintEvent*)
{
    QImage img("testImage.png"); // ��������� ��������
    QPainter painter(this); // ���������� ������ painter, ������� ������������ ���������
    painter.drawImage(0, 0, img.scaled(this->size())); // ������ ���� ����������� �� 0,0 � ����������� �� ����� �������
}

void MainWindow::CreateImage(QString path)
{
    QImage image(QSize(400, 300), QImage::Format_RGB32);
    QPainter painter(&image);
    painter.fillRect(QRectF(0, 0, 400, 300), Qt::white);
    painter.setPen(QPen(Qt::black));
    painter.drawLine(0, 0, image.width() / 2, image.height());
    painter.drawLine(image.width() / 2, image.height(), image.width(), 0);

    image.save("testImage.png");
}
void MainWindow::CreateImage2(QString path)
{
    QImage image(QSize(400, 300), QImage::Format_RGB32);
    QPainter painter(&image);
    painter.fillRect(QRectF(0, 0, 400, 300), Qt::white);
    painter.setPen(QPen(Qt::black));
    painter.drawLine(0, 0, image.width() / 4, image.height());
    painter.drawLine(image.width() / 4, image.height() , image.width() / 2, 0);
    painter.drawLine(image.width() / 2, 0, 3 * image.width() / 4, image.height());
    painter.drawLine(3*image.width() / 4, image.height(), image.width() , 0);

    image.save("testImage2.png");
}

void MainWindow::OpenImage(QString path)
{
    image_t1 = new QImage("testImage.png");
    QPoint qp;
    std::ofstream fout("data.txt", std::ios_base::out | std::ios_base::trunc);

    for (int i = 0; i < image_t1->width(); i++)
    {
        for (int j = 0; j < image_t1->height(); j++)
        {
            qp.setX(i);
            qp.setY(j);
            if (image_t1->pixel(qp) != 4294967295/* 4278190080*/)
            {
                x1.push_back(j);
                zeros1.push_back(abs(150-j));
                //qDebug() << j << " ";
                fout << j<< std::endl;
                break;
            }
        }

    }
    qDebug() <<"size:"<< x1.size() << " ";
    int sum = 0;
    for (int j = 0; j < zeros1.size(); j++)
    {
        sum += zeros1[j];
    }
    qDebug() << "sum1=" << sum << " ";
    fout.close();
    
    net.InitNeurons(x1, x1.size());
    net.LearnNeurons(10,0);
    //net.TestNeurons(0);
    for (size_t i = 0; i < net.l2[0]->SendW().size(); i++)
    {
        qp.setX(i);
        qp.setY(int(net.l2[0]->SendW()[i]));
        image_t1->setPixel(qp, Qt::red);
    }
    image_t1->save("testImage1-1-1-1-000000.png");
    
}
void MainWindow::OpenImage2(QString path)
{
    image_t2 = new QImage("testImage2.png");
    QPoint qp;
    std::ofstream fout("data.txt", std::ios_base::out | std::ios_base::trunc);

    for (int i = 0; i < image_t2->width(); i++)
    {
        for (int j = 0; j < image_t2->height(); j++)
        {
            qp.setX(i);
            qp.setY(j);
            if (image_t2->pixel(qp) != 4294967295/* 4278190080*/)
            {
                x2.push_back(j);
                fout << j << std::endl;
                zeros2.push_back(abs(150 - j));
                //qDebug() << j << " ";
                break;
            }
        }

    }
    qDebug() << "size:" << x2.size() << " ";
    int sum = 0;
    for (int j = 0; j < zeros2.size(); j++)
    {
        sum += zeros2[j];
    }
    qDebug()<<"sum2=" << sum << " ";
    fout.close();
    
    net.InitNeurons(x2, x2.size());
    net.LearnNeurons(10,1);
    qDebug() <<"v - v"<< net.TestNeurons(0, x1);
    qDebug() <<"v - w"<< net.TestNeurons(0, x2);
    qDebug() <<"w - v"<< net.TestNeurons(1, x1);
    qDebug() <<"w - w"<< net.TestNeurons(1, x2);
    for (size_t i = 0; i < net.l2[1]->SendW().size(); i++)
    {
        qp.setX(i);
        qp.setY(int(net.l2[1]->SendW()[i]));
        //qDebug() << int(net.l2[1]->SendW()[i]) << " ";// std::endl;
        image_t2->setPixel(qp, Qt::red);
    }
    image_t2->save("testImage2-2-2-2-000000.png");
    
}
