#include "NeuralNet.h"

void NeuralNet::InitNeurons(std::vector<double> x,int length)
{
	l1.push_back(new Neuron(x,length,1));
}
void NeuralNet::LearnNeurons(int steps,int i)
{
		l2.push_back(new Neuron(l1[i]->Send(), l1[i]->length, 2));
		l2[i]->Lerning(steps);
}
double NeuralNet::TestNeurons(int i, std::vector<double> xx)
{
	double res = 0.0 ;
	res = l2[i]->Thinking(xx);
	return res;
}
