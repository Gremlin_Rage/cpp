#include "Neuron.h"

Neuron::Neuron(std::vector<double> x,int length,int level)
{
	this->x = x;
	this->length = length;
	if (level == 2)
	{
		srand(time(NULL));
		min_ = 0;
		max_ = 300;
		for (size_t i = 0; i < length; i++)
		{
			x0.push_back(min_ + rand() % (max_ - min_ + 1)); 
			y.push_back(x0[i]);
			//std::cout << x0[i] << std::endl;
		}
	}
	this->level = level;
	this->error = 0.0;
}

std::vector<double> Neuron::Send()
{
	if (level == 1)
		return this->x;
	else
		return this->y;
}
std::vector<double> Neuron::SendW()
{
		return this->x0;
}
void Neuron::Lerning(int steps)
{
	float k = 0.75;
	for (size_t j = 0; j < steps; j++)
	{
		for (size_t i = 0; i < length; i++)
		{
			x0[i] = x0[i] + k* (x[i]-x0[i]);
		}
	}
}
double Neuron::Thinking(std::vector<double> xx)
{
	error = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		y[i] = abs(xx[i] - x0[i]);
		error += y[i];
	}
	return error;
}
