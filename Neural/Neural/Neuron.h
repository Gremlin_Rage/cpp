#pragma once
#include <ctime>
#include <iostream>
#include <vector>
class Neuron
{
public:
	std::vector<double> x,  y, x0;
	double error;
	Neuron(std::vector<double> x, int length, int level);
	int level, length, min_, max_;
	std::vector<double>  Send();
	void Lerning(int steps);
	double Thinking(std::vector<double> xx);

	std::vector<double> SendW();

};

