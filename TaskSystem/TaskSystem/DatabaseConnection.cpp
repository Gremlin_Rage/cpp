#include "DatabaseConnection.h"
DatabaseConnection::DatabaseConnection()
{
    for (int i = 0; i < 1025; i++)
        memset(task_[i].task, 0, 1023);
}
void DatabaseConnection::Connect()
{
    int libpq_ver = PQlibVersion();
    printf("Version of libpq: %d\n", libpq_ver);

    conn = PQconnectdb("user=postgres password=root host=127.0.0.1 dbname=postgres");
    if (PQstatus(conn) != CONNECTION_OK)
        terminate(1);
    PQsetNoticeProcessor(conn, processNotice, NULL);

    int server_ver = PQserverVersion(conn);
    char* user = PQuser(conn);
    char* db_name = PQdb(conn);

    printf("Server version: %d\n", server_ver);
    printf("User: %s\n", user);
    printf("Database name: %s\n", db_name);
}
void DatabaseConnection::StandartQuery()
{
    res = PQexec(conn, "SELECT * FROM table_workers;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        terminate(1);

    int ncols = PQnfields(res);
    numCol = ncols;
    printf("There are %d columns:", ncols);
    namescol.clear();
    for (int i = 0; i < ncols; i++)
    {
        char* name = PQfname(res, i);
        namescol.push_back(name);
        printf(" %s", name);
    }
    printf("\n");

    int nrows = PQntuples(res);
    numRow = nrows;
    Worker* worker_ = new Worker[nrows];
    for (int i = 0; i < nrows; i++)
    {
        worker_[i].id = atoi(PQgetvalue(res, i, 0));
        strcpy(worker_[i].name, PQgetvalue(res, i, 1));
        strcpy(worker_[i].position, PQgetvalue(res, i, 2));
        cout << " Id: " << worker_[i].id;
        cout << " Name: " << worker_[i].name;
        cout << " Position: " << worker_[i].position;
        vWorker.push_back(worker_[i]);
    }

    printf("Total: %d rows\n", nrows);

    clearRes();

}
void DatabaseConnection::Query()
{
    res = PQexec(conn, "SELECT tt1.*, string_agg(tt.task, E'\n') as oldtask FROM table_tasks tt1 join table_tasks  tt on tt1.executor = tt.executor and (tt1.date_death_line > now() and tt.date_death_line < now()) group by tt1.id;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        terminate(1);

    int ncols = PQnfields(res);
    numCol = ncols;
    printf("There are %d columns:", ncols);
    namescol.clear();
    for (int i = 0; i < ncols; i++)
    {
        char* name = PQfname(res, i);
        namescol.push_back(name);
      //  printf(" %s", name);
    }
    //printf("\n");

    int nrows = PQntuples(res);
    numRow = nrows;
    //task_ = new Task[nrows];
    ddl = new char*[nrows], ddl2 = new char* [nrows];
    //char** date_string = new char*[nrows], ** date_string2 = new char*[nrows];
    for (int i = 0; i < nrows; i++)
    {
        task_[i].id = atoi(PQgetvalue(res, i, 0));
        strcpy(task_[i].task , PQgetvalue(res, i, 1));
        task_[i].parentTask = atoi(PQgetvalue(res, i, 2));
        strcpy(task_[i].executor , PQgetvalue(res, i, 3));
        ddl[i] = new char[100];       
        strcpy(ddl[i], PQgetvalue(res, i, 4));       
        ddl[i][4] = ddl[i][7] = '\0';
        task_[i].dateDeathLine->tm_year = atoi(&ddl[i][0]) - 1900;
        task_[i].dateDeathLine->tm_mon = atoi(&ddl[i][5]) - 1;
        task_[i].dateDeathLine->tm_mday = atoi(&ddl[i][8]);

        ddl2[i] = new char[100];        
        strcpy(ddl2[i] , PQgetvalue(res, i, 5));        
        ddl2[i][4] = ddl2[i][7] = '\0';
        task_[i].dateStart->tm_year = atoi(&ddl2[i][0]) - 1900;
        task_[i].dateStart->tm_mon = atoi(&ddl2[i][5]) - 1;
        task_[i].dateStart->tm_mday = atoi(&ddl2[i][8]);        
        strcpy(task_[i].status , PQgetvalue(res, i, 6));        
        strcpy(task_[i].oldtask , PQgetvalue(res, i, 7));
        vTask[i]=task_[i]
            ;
        /*
        cout << " Id: " << task_[i].id;
        cout << " Task: " << task_[i].task;
        cout << " Parent: " << task_[i].parentTask;
        cout << " Executor: " << task_[i].executor;
        date_string[i] = new char[50];
        date_string2[i] = new char[50];
        strftime(date_string[i], 50, " %Y-%m-%d", task_[i].dateDeathLine);
        cout << " DeathLine: " << date_string[i];

        strftime(date_string2[i], 50, "%Y-%m-%d", task_[i].dateStart);
        cout << " Start: " << date_string2[i];
        cout << " Status: " << task_[i].status;
        cout << " Oldtask: " << task_[i].oldtask << endl;
        */
    }

    printf("Total: %d rows\n", nrows);

    clearRes();

}

void DatabaseConnection::DeleteQuery(int id)
{
    std::string query = "DELETE FROM table_tasks where id=" + std::to_string(id) + ";";
    res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
        terminate(1);
    clearRes();
}

void DatabaseConnection::AddQuery(std::string task,int parent,std::string executor,tm* date_death_line)
{

    const char* query =
        "INSERT INTO table_tasks (id,task,parent_task, executor,date_death_line,date_start,status) VALUES ((Select max(id)+1 from table_tasks),$1, $2 ,$3,$4, now(), 'Appointed');";
    const char* params[4];
    const char* t = task.c_str();
    char *number = new char[10];
    itoa(parent, number, 10);
    const char* p = number;
    const char* e = executor.c_str();
    char* date_string = new char[50];
    strftime(date_string, 50, " %Y-%m-%d", date_death_line);
    params[0] = t;
    params[1] = p;
    params[2] = e;
    params[3] = date_string;

    res = PQexecParams(conn, query, 4, NULL, params,
    NULL, NULL, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
        terminate(1);
    clearRes();
}
void DatabaseConnection::processNotice(void* arg, const char* message)
{
    UNUSED(arg);
    UNUSED(message);
    // do nothing
}

#include <fstream> // ������ � �������
#include <iomanip> 
void DatabaseConnection::terminate(int code)
{
    setlocale(LC_ALL, "rus");

    // ��������� ������ � ������, ��� ���� ���� ��������� � ������ ������, �������������� ������ ��� ������ �� ����
    ofstream fout("errors.txt", ios_base::out | ios_base::trunc);

    if (!fout.is_open()) // ���� ���� ����� ������
    {
        cout << "���� �� ����� ���� ������ ��� ������\n"; // ���������� ��������������� ���������
         // ��������� ����� �� ���������
    }
    fout << PQerrorMessage(conn);
    fout.close();
    if (code != 0)
        fprintf(stderr, "%s\n", PQerrorMessage(conn));
    if (res != NULL)
        PQclear(res);

    if (conn != NULL)
        PQfinish(conn);

    exit(code);
}

void DatabaseConnection::clearRes()
{
    PQclear(res);
    res = NULL;
}
DatabaseConnection::~DatabaseConnection()
{
    terminate(0);
}
