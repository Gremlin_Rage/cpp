#pragma once

#include <QtWidgets/qwidget.h>

//#include <QtGui/QtGui>

#include <QtWidgets/qtablewidget.h>
#include <QtWidgets/qlabel.h>
#include <QtWidgets/qtextedit.h>
#include <QtWidgets/qpushbutton.h> 

#include "DatabaseConnection.h"

#pragma comment(lib,"Qt6Cored.lib")
#pragma comment(lib,"Qt6Widgetsd.lib")
class MyWidget
{
public:
	QWidget self; 
	QTableWidget tableworkers;
	QTableWidget tabletasks;
	QLabel labelworkers;
	QLabel labeltasks;
	QLabel labeldelete, labeltask, labelparenttask, labelexecutor, labeldeath;
	QTextEdit editid, edittask, editparenttask, editexecutor, editdeath;
	QPushButton buttondelete, buttonrefresh,addbutton;
	MyWidget();
	DatabaseConnection db;
	void InitWidget();
	void Tape(QTableWidget &table,bool t);
	~MyWidget();
};

