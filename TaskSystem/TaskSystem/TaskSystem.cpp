﻿// TaskSystem.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <QtWidgets/QApplication>
#include "MyWidget.h"
using namespace std;
int main(int argc, char* argv[])
{
    
    std::time_t now = std::time(0);
    tm* ltm = localtime(&now);
    cout << "Year" << 1900 + ltm->tm_year << endl;
    cout << "Month: " << 1 + ltm->tm_mon << endl;
    cout << "Day: " << ltm->tm_mday << endl;
    cout << "Time: " << ltm->tm_hour << ":";
    cout << ltm->tm_min << ":";
    cout << ltm->tm_sec << endl;
    
    //db.AddQuery("Bla",0,"Ivanov Ivan Ivanovich",ltm);
    //db.AddQuery("Hell", 0, "Ivanov Ivan Ivanovich",ltm);
    QApplication a(argc, argv);
    MyWidget mw;
    mw.InitWidget();
    return a.exec();
}
