#include "MyWidget.h"
MyWidget::MyWidget()
{
}
void MyWidget::InitWidget()
{
    
    db.Connect();
    
    self.setWindowTitle("Task system");
    self.setGeometry(50, 50, 1000, 700);
   
    db.StandartQuery();
    labelworkers.setText("Workers");
    labelworkers.setGeometry(0, 0, 50, 30);
    labelworkers.setParent(&self);
    tableworkers.setParent(&self);
    tableworkers.setColumnCount(db.numCol);
    tableworkers.setRowCount(db.numRow);
    tableworkers.setGeometry(0, 30, 950, 150);
    Tape(tableworkers,false);
    
    db.Query();

    labeltasks.setText("Tasks");
    labeltasks.setParent(&self);
    labeltasks.setGeometry(0, 160, 50, 30);
    tabletasks.setParent(&self);
    tabletasks.setColumnCount(db.numCol);
    tabletasks.setRowCount(db.numRow);
    tabletasks.setGeometry(0, 200, 950, 150);
    Tape(tabletasks, true);

    labeldelete.setText("Input number id task to delete and click delete");
    labeldelete.setParent(&self);
    labeldelete.setGeometry(0, 400, 250, 30);

    editid.setParent(&self);
    editid.setGeometry(260, 400, 30, 30);

    buttondelete.setText("delete task");
    buttondelete.setParent(&self);
    buttondelete.setGeometry(300, 400, 100, 30);
    QObject::connect(&buttondelete, &QPushButton::clicked, [=]() {
        //buttondelete.setText("Hi");
        db.DeleteQuery(editid.toPlainText().toInt());
        InitWidget();
        });

    buttonrefresh.setText("refresh");
    buttonrefresh.setParent(&self);
    buttonrefresh.setGeometry(450, 400, 100, 30);
    QObject::connect(&buttonrefresh, &QPushButton::clicked, [=]() {
        //buttonrefresh.setText("Hi");
        InitWidget();
        });

    labeltask.setText("Input task");
    labeltask.setParent(&self);
    labeltask.setGeometry(0, 500, 150, 30);

    edittask.setParent(&self);
    edittask.setGeometry(0, 550, 150, 30);
    edittask.acceptRichText();

    labelparenttask.setText("Input id parent task(0 - not parent)");
    labelparenttask.setParent(&self);
    labelparenttask.setGeometry(160, 500, 250, 30);


    editparenttask.setParent(&self);
    editparenttask.setGeometry(160, 550, 50, 30);

    labelexecutor.setText("Input executor");
    labelexecutor.setParent(&self);
    labelexecutor.setGeometry(410, 500, 150, 30);


    editexecutor.setParent(&self);
    editexecutor.setGeometry(410, 550, 150, 30);

    labeldeath.setText("Input date death line(2022-12-31)");
    labeldeath.setParent(&self);
    labeldeath.setGeometry(570, 500, 250, 30);



    editdeath.setParent(&self);
    editdeath.setGeometry(570, 550, 250, 30);

    addbutton.setText("add task");
    addbutton.setParent(&self);
    addbutton.setGeometry(250, 600, 100, 30);
    QObject::connect(&addbutton, &QPushButton::clicked, [=]() {
        //addbutton.setText("Hi");
        char* ddl = new char[50];
        tm date;
        strcpy(ddl, editdeath.toPlainText().toStdString().c_str());
        ddl[4] = ddl[7] = '\0';
        date.tm_year = atoi(&ddl[0]) - 1900;
        date.tm_mon = atoi(&ddl[5]) - 1;
        date.tm_mday = atoi(&ddl[8]);
        db.AddQuery(edittask.toPlainText().toStdString(), editparenttask.toPlainText().toInt(), editexecutor.toPlainText().toStdString(), &date);
        InitWidget();
        });

    self.show();
}

void MyWidget::Tape(QTableWidget &table,bool t)
{
    QStringList list;
    QString qstr;
    for (int i = 0; i < db.namescol.size(); i++)
    {
        qstr = QString::fromStdString(db.namescol[i]);
        list.push_back(qstr);
    }
    table.setHorizontalHeaderLabels(list);
    vector<vector<string>> v;
    vector<string> tmp;
    const int n = 64;
    char* date_d = new char[n],
        * date_s = new char[n], * strid = new char[n], * strptask = new char[n];
    char*strid_ = new char[n];
    memset(date_d, 0, n);
    if (t)
    {
        for (int i = 0; i < db.numRow; i++)
        {
            itoa(db.vTask[i].id, strid, 10);
            tmp.push_back(strid);
            tmp.push_back(db.vTask[i].task);
            itoa(db.vTask[i].parentTask, strptask, 10);
            tmp.push_back(strptask);
            tmp.push_back(db.vTask[i].executor);
            strftime(date_d, n, " %Y-%m-%d", db.vTask[i].dateDeathLine);
            tmp.push_back(date_d);
            strftime(date_s, n, "%Y-%m-%d", db.vTask[i].dateStart);
            tmp.push_back(date_s);
            tmp.push_back(db.vTask[i].status);
            tmp.push_back(db.vTask[i].oldtask);
            v.push_back(tmp);
            tmp.clear();
        }
    }
    else
    {
        for (int i = 0; i < db.vWorker.size(); i++)
        {
            itoa(db.vWorker[i].id, strid_, 10);
            tmp.push_back(strid_);
            tmp.push_back(db.vWorker[i].name);
            tmp.push_back(db.vWorker[i].position);
            v.push_back(tmp);
            tmp.clear();
        }
    }
    for (int i = 0; i < v.size(); i++)
        for (int j = 0; j < v[i].size(); j++)
        {
            qstr = QString::fromStdString(v[i][j]);
            table.setItem(i, j, new QTableWidgetItem(qstr));
        }
}
MyWidget::~MyWidget()
{
}