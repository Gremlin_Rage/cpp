#pragma once

#include <ctime>
#include <cstring>
#pragma warning(disable : 4996)
class Task
{
public:
	int id;
	char* task;
	int parentTask;
	char* executor;
	int idExecutor;
	tm* dateStart;
	tm* dateDeathLine;
	char* status;
	char* oldtask;
	Task();
	void AddTask(char* task_, int parentTask_, char* executor_,
		int idExecutor_, tm* dateDeathLine_);
	void DeleteTask(int num);
	~Task();
};

