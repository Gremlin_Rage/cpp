#pragma once
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <libpq-fe.h>
#include <string>
#include <vector>
#include "Task.h"
#include "Worker.h"
#pragma comment(lib,"libpq.lib")
#define UNUSED(x) (void)(x)
using namespace std;
class DatabaseConnection
{
public:
    PGconn* conn = NULL;
    PGresult* res = NULL;
    int numCol,numRow;
    vector<string> namescol;
    Task vTask[1025];
    vector<Worker> vWorker;
    Task task_[1025];
    char **ddl, **ddl2;
    DatabaseConnection();
    void Connect();
    void terminate(int code);
    void clearRes();
    static void processNotice(void* arg, const char* message);
    void Query();
    void StandartQuery();
    void AddQuery(std::string task, int parent, std::string executor, tm* date_death_line);
    void DeleteQuery(int id);
    ~DatabaseConnection();
};

