Create table table_workers(id serial primary key,name text unique,position text); 
CREATE TABLE table_tasks(id serial primary key,task text,parent_task integer,executor text,date_death_line date,date_start date,status text,foreign key (parent_task) references table_tasks(id),foreign key (executor) references table_workers(name));
Insert into table_workers values(1,'Ivanov Ivan Ivanovich','Developer');
Insert into table_workers values(2,'Petrov Petr Petrovich','Tester');
Insert into table_tasks values(1,'Add personal cabinet on site',NULL,'Ivanov Ivan Ivanovich','2023-03-01','2023-04-19','Appointed');
Insert into table_tasks values(2,'Test personal cabinet on site',NULL,'Petrov Petr Petrovich','2023-04-01','2023-05-02','Appointed');
Insert into table_tasks values(3,'Add payment system',NULL,'Ivanov Ivan Ivanovich','2023-07-01','2023-04-19','Appointed');
Insert into table_tasks values(4,'Test payment system',NULL,'Petrov Petr Petrovich','2023-06-01','2023-04-19','Appointed');
Insert into table_tasks values(0,'Without parent task',NULL,NULL,'2023-05-01','2023-04-19','Appointed');