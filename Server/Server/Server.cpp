﻿/*
Добрый день.
Решение желательно разместить в репозитории Github и прислать ссылки. Но можно прислать исходные коды.
Тестовая задача 1. Язык С++. Среда разработки Visual Studio 2019.
Задача. Написать 2 консольных приложения Client.exe и Server.exe под Windows, обменивающихся файлами через UPD сокет с подтверждением по TCP.
1.Сервер. Server.exe
Первый параметр – IP, второй номер порта, третий – каталог для хранения файлов.
Сервер начинает прослушивать по IP адресу порт и ждет подключения клиента по TCP.
Пример:
Server.exe 127.0.0.1 5555 temp
1.      Клиент. Client.exe
Старт с 5 параметрами.
1 и 2 параметры – IP адрес и порт для подключения к серверу. Третий – порт для отправки UDP пакетов. Четвертый – путь к файлу. Пятый – таймаут на подтверждение UDP пакетов в миллисекундах.
Пример
Client.exe 127.0.0.1 5555 6000 test.txt 500
3.Взаимодействие сервера и клиента.
При старте сервер открывает TCP сокет с IP адресом и портом из стартовых параметров(1 и 2 параметры), берет его на прослушивание и ожидает подключений от клиента.
Клиент при старте пытается подключиться к серверу по TCP на IP адрес и порт из стартовых параметров (1 и 2 параметры ) пока не будет установлено соединение. После установления соединения клиент загружает в память файл (3 параметр, размер файла не более 10 Мб.) и отправляет по TCP соединению имя файл и порт UDP на сервер.
Далее клиент начинает отправлять файл блоками (Каждый блок данных в udp пакетах должен содержать свой id)  udp датаграммами и получать по TCP подтверждения о приеме сервером.
В случае если в течении таймаута (5 параметр) не было получено подтверждение на пакет, то пакет по udp отправляется повторно. В случае когда все пакеты подтверждены клиент уведомляет сервер TCP об окончании передачи файла,
закрывает TCP соединение, завершает работу.
Сервер после установления подключения клиента по ТСP, получения имя файла и порта udp открывает udp сокет и начинает принимать udp пакеты исходящие с IP адреса клиента и порта переданного клиентом.
На каждый полученный udp пакет сервер отправляет подтверждение через tcp сокет на клиент. Полученные пакеты хранятся в памяти. После получения от клиента уведомления об окончании передачи (все пакеты подтверждены на клиенте) сервер сохраняет файл в каталог (3 параметр).
Формат посылок по TCP (имя файла, порт и подтверждения пакетов) - на ваше усмотрение
формат и нумерация блоков для udp пакетов - на ваше усмотрение
*/
#include <iostream>
#include "Connection.h"
#include <fstream>
#include <string>
int main(int argc, char* argv[])
{
    system("Del \"C:\\temp\\test.txt\"");
    if (argc == 1)
    {
        std::cout << "no flags argc!" << std::endl;
        std::cout << "Usage: Server.exe <ip> <port> <Folder>" << std::endl;
        exit(1);
    }
    std::string timeout_ = "1000";
    Connection* con = new Connection(timeout_.c_str());
    con->InitServer(argv[1], atoi(argv[2]));
    con->ReceiveServer();
    int portudp = atoi(con->GetBuffer());
    con->InitServerUDP(argv[1], portudp);
    con->ReceiveServer();
    std::string FileName = con->GetBuffer();
    if (FileName.size() > FILENAME_MAX)
        std::cout << "File name is so long" << std::endl;
    con->ReceiveServer();
    int j = atoi(con->GetBuffer());
    int i = 0,jj;
    std::string id = "";
    std::cout << "Starting transmission" << std::endl;
    while (i < j)
    {
        jj = 0;
        con->ReceiveServerUDP();
        while (con->GetVec()[i][jj++] != 'n')
            ;
        while (con->GetVec()[i][jj] != 'E')
            id += con->GetVec()[i][jj++];
        con->SendServer(id);
        std::cout << "i="<<i << std::endl;
        if (atoi(id.c_str()) == i)
        {
            i++;      
        }
        id = "";
    }
    std::cout << "Transmission is end" << std::endl;
    std::ofstream out;          // поток для записи
    std::string path = argv[3];
    system("mkdir \"C:\\temp\\\"");
    path += '\\' + FileName;
    if (path.size() > FILENAME_MAX)
        std::cout << "File name is so long" << std::endl;
    std::cout << "Creating file" << std::endl;
    out.open(path); // открываем файл для записи
    if (out.is_open())
    {
        std::string strfile = "";
        int jj;
        for (int i = 0; i < con->GetVec().size(); i++)
        {
            strfile = "";
            jj = 0;
            while (con->GetVec()[i][jj++] != 'd')
                ;
            for (jj; jj < con->GetVec()[i].size(); jj++)
                strfile += con->GetVec()[i][jj];
            out <<strfile;
        }
        out.close();
    }
    con->ServerClose();
    con->~Connection();
    std::cout << "Done!\n";
}
