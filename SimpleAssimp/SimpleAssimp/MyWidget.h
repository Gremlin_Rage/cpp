#pragma once
#include <QtCore/qglobal.h>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>
#include <QtWidgets/QPushButton>
#include <QtCore/QTimer>
#include<QtOpenGLWidgets/qopenglwidget.h>
#include <QtOpenGL/QOpenGLVersionProfile>
#include <QtWidgets/QWidget>
#include <QtOpenGL/qopengltexture.h>
#include <iostream>
#include <fstream>
#include <algorithm>
class MyWidget :
    public QOpenGLWidget
{
public:
    MyWidget(QWidget* parent);
    void initializeGL();
    void resizeGL(int nWidth, int nHeight);
    void paintGL();
    QOpenGLContext* ctx;
    GLuint shaderProgram;
    QOpenGLExtraFunctions* f;
    GLuint vao, id;
    GLuint* indices;
    QOpenGLTexture* texture;
    QImage image1;
    GLuint buffer;
    GLfloat rotate__ = 0;
    GLuint EBO;
private slots:
    void change();
    QTimer *timer;
};

