#include "MyWidget.h"
const GLchar* vertexShaderSource = "#version 330 core\n"
"layout(location = 0) in vec3 position;\n"
"layout(location = 1) in vec4 color;\n"
"layout(location = 2) in vec2 texCoord;\n"
"out vec4 ourColor;\n"
"out vec2 TexCoord;\n"
"uniform mat4 transform;\n"
"void main()\n"
"{\n"
"gl_Position = transform*vec4(position, 1.0f);\n"
"ourColor = color;\n"
"TexCoord = texCoord;\n"
"}\0";
const GLchar* fragmentShaderSource = "#version 330 core\n"
"in vec4 ourColor;\n"
"in vec2 TexCoord;\n"
"out vec4 color;\n"
"uniform sampler2D ourTexture;\n"
"void main()\n"
"{\n"
"color = texture(ourTexture, TexCoord)+ourColor;\n"
"}\0";
#pragma comment(lib,"Qt6Cored.lib")
#pragma comment(lib,"Qt6Guid.lib")
#pragma comment(lib,"Qt6Widgetsd.lib")
#pragma comment(lib,"Qt6OpenGLd.lib")
#pragma comment(lib,"Qt6OpenGLWidgetsd.lib")
#pragma comment(lib,"OpenGL32.lib")
MyWidget::MyWidget(QWidget* parent)
{
	this->setWindowTitle(QString("Window"));
	rotate__ = 0.0;
	timer = new QTimer;
	connect(timer, &QTimer::timeout, this, &MyWidget::change);
	timer->start(10);
}
void MyWidget::initializeGL()
{
	ctx = new QOpenGLContext();
	f = QOpenGLContext::currentContext()->extraFunctions();
	//Build and compile our shader program
	//Vertex shader
	GLuint vertexShader = f->glCreateShader(GL_VERTEX_SHADER);
	f->glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	f->glCompileShader(vertexShader);
	// Check for compile time errors
	GLint success;
	GLchar infoLog[512];
	f->glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		f->glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// Fragment shader
	GLuint fragmentShader = f->glCreateShader(GL_FRAGMENT_SHADER);
	f->glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	f->glCompileShader(fragmentShader);
	// Check for compile time errors
	f->glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		f->glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// Link shaders
	/*GLuint */ shaderProgram = f->glCreateProgram();
	f->glAttachShader(shaderProgram, vertexShader);
	f->glAttachShader(shaderProgram, fragmentShader);
	f->glLinkProgram(shaderProgram);
	// Check for linking errors
	f->glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		f->glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	f->glDeleteShader(vertexShader);
	f->glDeleteShader(fragmentShader);
	GLfloat *buf = new GLfloat[197820*9];
	/*
	GLfloat buf[54] =
	{
		-1.0,  1.0, 0.0,  1.0, 0.0, 0.0, 1.0, 0.0, 1.0,
		-1.0, -1.0, 0.0,  1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
		 1.0, -1.0, 0.0,  1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
		 1.0, -1.0, 0.0,  1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
		 1.0,  1.0, 0.0,  1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
		-1.0,  1.0, 0.0,  1.0, 0.0, 0.0, 1.0, 0.0, 1.0
	};
	*/
	GLfloat x, y, z,r=0.5;
	int count = 0;
	GLfloat arr1[9];
	int n;
	bool b = true;
	for (GLfloat i = 0; i <= 3.14; i += 0.01)
		for (GLfloat j = 0; j <= 2.0 * 3.14; j += 0.01)
		{
			x = r * (GLfloat)sin(i) * (GLfloat)cos(j);
			y = r * (GLfloat)sin(i) * (GLfloat)sin(j);
			z = r * (GLfloat)cos(i);
			/*
			if (x>0.2 && y>0.2 && z>0.2)
			{
				qDebug() << x;
				qDebug() << y;
				qDebug() << z;
			}
			*/
			arr1[0] = x;
			arr1[1] = y;
			arr1[2] = z;
			arr1[3] = GLfloat(rand()) / RAND_MAX;
			arr1[4] = GLfloat(rand()) / RAND_MAX;
			arr1[5] = GLfloat(rand()) / RAND_MAX;
			arr1[6] = 1.0;
			arr1[7] = 0.0;
			arr1[8] = 0.0;
			n = sizeof(arr1) / sizeof(arr1[0]);
			std::copy(arr1, arr1 + n, buf + n * count);
			count++;
			/*
			// Copy first array to second array
			if (b)
			{
				b = false;
				std::copy(arr1, arr1 + n, buf);
			}
			else
				std::copy(arr1, arr1 + n, buf+1);
				*/
		}
	//1577536


	// Second array to store the elements of the first array
	
	std::cout << "n"<<n<<"ct" << count<<"bf"<< sizeof(GLfloat) * 197820 *9;
	f->glGenVertexArrays(1, &vao);
	f->glGenBuffers(1, &buffer);
	f->glBindVertexArray(vao);
	f->glBindBuffer(GL_ARRAY_BUFFER, buffer);
	//f->glBufferData(GL_ARRAY_BUFFER, sizeof(buf), buf, GL_STATIC_DRAW);
	f->glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 197820 *9, buf, GL_STATIC_DRAW);
	/*
	indices = new GLuint[6];
	for (int i = 0; i < 6; i++)
		indices[i] = (GLuint)i;
	f->glGenBuffers(1, &EBO);
	f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	f->glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	*/
	
	f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)0);
	f->glEnableVertexAttribArray(0);
	f->glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	f->glEnableVertexAttribArray(1);
	f->glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)(7 * sizeof(GLfloat)));
	f->glEnableVertexAttribArray(2);
	f->glEnableVertexAttribArray(0);
	f->glBindBuffer(GL_ARRAY_BUFFER, 0);
	f->glBindVertexArray(0);
	std::string path_to_image = "C:\\Users\\User\\Desktop\\cpp\\SimpleAssimp\\SimpleAssimp\\28.png";
	image1.load(QString::fromStdString(path_to_image));
	if (image1.isNull())
		std::cout<<"Not found:" << path_to_image;
	/*
	std::ifstream iff(path_to_image);
	if (iff.bad() == true) 
		std::cout << "file is not present"; 
	else 
		std::cout << "file is present";
	*/
	texture = new QOpenGLTexture(image1.mirrored(true,false));
	texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
	texture->setMagnificationFilter(QOpenGLTexture::Linear);
	std::cout << image1.format() << std::endl;
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image1.width(), image1.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, image1.bits());
	if (glGetError())
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image1.width(), image1.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, image1.bits());
	if (glGetError())
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image1.width(), image1.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, image1.bits());
	if (glGetError())
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image1.width(), image1.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, image1.bits());
	glBindTexture(GL_TEXTURE_2D, 0);
}
void MyWidget::resizeGL(int nWidth, int nHeight)
{
	glViewport(0, 0, nHeight, nHeight);
	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	glClearColor(0, 0, 0, 1);
}
void MyWidget::change()
{
	//rotate+=1;
	update();
}
void MyWidget::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	f->glUseProgram(shaderProgram);
	f->glBindVertexArray(buffer);
	glPushMatrix();
	rotate__ += 1.0;
	//std::cout << rotate__ << std::endl;
	GLfloat transX[16] =
	{
		1.0,0.0,0.0,0.0,
		0.0,1.0 * cos(rotate__ * 0.0175),0.0 - sin(rotate__ * 0.0175),0.0,
		0.0,0.0 + sin(rotate__ * 0.0175),1.0 * cos(rotate__ * 0.0175),0.0,
		0.0,0.0,0.0,1.0
	};

	GLfloat transY[16] =
	{
		1.0 * cos(rotate__ * 0.0175),0.0,0.0 + sin(rotate__ * 0.0175),0.0,
		0.0,1.0,0.0,0.0,
		0.0 - sin(rotate__ * 0.0175),0.0,1.0 * cos(rotate__ * 0.0175),0.0,
		0.0,0.0,0.0,1.0
	};
	GLfloat transZ[16] =
	{
		1.0 * cos(rotate__ * 0.0175),0.0 - sin(rotate__ * 0.0175),0.0,0.0,
		0.0 + sin(rotate__ * 0.0175),1.0 * cos(rotate__ * 0.0175),0.0,0.0,
		0.0,0.0,1.0,0.0,
		0.0,0.0,0.0,1.0
	};
	GLfloat transZ180[16] =
	{
		1.0 * cos(180.0 * 0.0175),0.0 - sin(180.0 * 0.0175),0.0,0.0,
		0.0 + sin(180.0 * 0.0175),1.0 * cos(180.0 * 0.0175),0.0,0.0,
		0.0,0.0,1.0,0.0,
		0.0,0.0,0.0,1.0
	};
	GLfloat transZero[16] =
	{
		1.0,0.0,0.0,0.0,
		0.0,1.0,0.0,0.0,
		0.0,0.0,1.0,0.0,
		0.0,0.0,0.0,1.0
	};
	unsigned int transformLoc = f->glGetUniformLocation(shaderProgram, "transform");
	//f->glUniformMatrix4fv(transformLoc, 1, GL_FALSE, transZ);
	//f->glUniformMatrix4fv(transformLoc, 1, GL_FALSE, transY);
	//f->glUniformMatrix4fv(transformLoc, 1, GL_FALSE, transX);
	f->glUniformMatrix4fv(transformLoc, 1, GL_FALSE, transZ180);
	//glBindTexture(GL_TEXTURE_2D, id);
	f->glDrawArrays(GL_POINTS, 0, 197820);
	f->glBindVertexArray(0);
	glPopMatrix();
}