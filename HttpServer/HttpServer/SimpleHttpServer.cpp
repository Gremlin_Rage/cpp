#include "SimpleHttpServer.h"
#pragma comment(lib,"Qt6HttpServerd.lib")
#pragma comment(lib,"Qt6Cored.lib")
#pragma comment(lib,"Qt6Networkd.lib")
#pragma comment(lib,"Qt6WebSocketsd.lib")
int SimpleHttpServer::run()
{
    httpServer.route("/public/<arg>", [](const QUrl& url) {
        return QHttpServerResponse::fromFile("public/" + url.path());
        });
    httpServer.route("/", QHttpServerRequest::Method::Get, [](const QHttpServerRequest& request) {
        const QString index = "public\\index.html";
        return QHttpServerResponse::fromFile(index);
        });
    httpServer.route("/json", QHttpServerRequest::Method::Get,[](const QHttpServerRequest& request) {
            QJsonObject json
            {
                {"email","jane.doe@qt.io"},
                {"first_name" , "Jane" },
                {"last_name", "Doe"},
                {"avatar", "/img/faces/1-image.jpg"}
            };
            return QHttpServerResponse(json);
        }
    );
    httpServer.route("/alert", QHttpServerRequest::Method::Post, [](const QHttpServerRequest& request) {
        QString byte = QString::fromLocal8Bit(request.body().data());
        int beg = byte.indexOf("name=");
        QString name = "";
        for (uint i = beg+5; i < byte.size(),byte[i]!=QString("&"); i++)
            name += byte[i];
        return "<script>alert('Hi "+name+"');</script>";
        });
    static const auto Port = httpServer.listen(QHostAddress("localhost"),80);
    return 0;
}
