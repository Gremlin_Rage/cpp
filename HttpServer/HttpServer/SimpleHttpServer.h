#pragma once
#include <QtHttpServer/qhttpserver.h>
#include <QtCore/qcoreapplication.h>
#include <QtCore/qfile.h>
#include <QtCore/qjsonobject.h>
class SimpleHttpServer
{
	//Q_OBJECT
public:
	int run();
private:
	QHttpServer httpServer;
};

